export CVSROOT=anoncvs@ftp.hostserver.de:/cvs
export DOWN=$HOME/Downloads
export NERD=$HOME/Nerdices
export REPOS=$NERD/dev/repos
export MYPORTS=/usr/ports/mystuff

alias ll="ls -lsAh"
alias dfh="df -h"
alias mk4="MAKE_JOBS=4 make build"
alias pldc="make port-lib-depends-check"
alias pclean="make clean=all"
alias pcheck="ksh /usr/ports/infrastructure/bin/portcheck "
alias plogger="perl /usr/ports/infrastructure/bin/portslogger ."
